import { View, Text, ActivityIndicator } from "react-native";
import React, {useContext}from "react";

import {NavigationContainer} from '@react-navigation/native';
import AuthStack from "./AuthStack";
import HomeScreen from "../screens/HomeScreen";
import { AuthContext, AuthProvider } from "../context/AuthContext";
import styles from "../screens/styles";

const  AppNav = () => {
    const {isLoading, userToken} = useContext(AuthContext);
    
    if(isLoading) {
        <View style= {styles.isLoadingstyle}>
            <ActivityIndicator size={'large'}/> 
        </View>
    }

    return(
        <NavigationContainer>
            {userToken !== null ? <HomeScreen/> : <AuthStack/> }
        </NavigationContainer>
    
    
    )
}

export default AppNav