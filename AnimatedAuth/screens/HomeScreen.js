import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useState} from 'react';
import {deviceHeight, deviceWidth} from './Dimensions';
import Icon from 'react-native-vector-icons/Ionicons';
import Cards from './Cards';
import { useNavigation } from '@react-navigation/native';

function HomeScreen (props){
  console.log(props);
  const [city, setCity] = useState('');
  const navigation = useNavigation();
  const cities = [
    {
      name: 'New Delhi',
      image: require('../assets/image3.jpeg'),
    },
    {
      name: 'New York',
      image: require('../assets/image4.jpeg'),
    },
    {
      name: 'London',
      image: require('../assets/image5.jpeg'),
    },
    {
      name: 'San Francisco',
      image: require('../assets/image6.jpeg'),
    },
    {
      name: 'New Jersey',
      image: require('../assets/image7.jpeg'),
    },
  ];
  return (
    <View > 
    <ImageBackground
      source={require('../assets/image2.jpeg')}
      style={{height: deviceHeight, width: deviceWidth}}
      imageStyle={{opacity: 0.6, backgroundColor: 'black'}}
    />
    <View
      style={{
        position: 'absolute',
        paddingVertical: 50,
        paddingHorizontal: 10,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: deviceWidth - 20,
        }}>
        <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
              <Icon name="arrow-back-outline" size={22} color="white" />
        </TouchableOpacity>
        <Image
          source={require('../assets/user.jpeg')}
          style={{height: 46, width: 46, borderRadius: 50}}
        />
      </View>

      <View style={{paddingHorizontal: 20, marginTop: 100}}>
        <Text style={{fontSize: 40, color: 'white'}}>Hello S.G.</Text>
        <Text style={{color: 'white', fontSize: 22, fontWeight: 'bold'}}>
          Search the city by the name
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            borderRadius: 20,
            borderWidth: 1,
            borderColor: 'white',
            marginTop: 16,
            paddingHorizontal: 10,
            height:40,
          }}>
          <TextInput
            value={city}
            onChangeText={val => setCity(val)}
            placeholder="Search City"
            placeholderTextColor="white"
            style={{paddingHorizontal: 10, color: 'white', fontSize: 16}}
          />
           <TouchableOpacity onPress={() => props.navigation.navigate('Details', {name: city})}>
              <Icon name="search" size={22} color="white" />
            </TouchableOpacity>
        </View>

        <Text style={{color: 'white', fontSize: 25, paddingHorizontal: 10, marginTop: 220, marginBottom: 20}}>
          My Locations
        </Text>
        <FlatList
          horizontal
            data={cities}
            renderItem={({item}) => (
              <Cards name={item.name} image={item.image} navigation={props.navigation} />
            )}
          
        />
      </View>
    </View>
  </View>  
  
  );
}

export default HomeScreen;
