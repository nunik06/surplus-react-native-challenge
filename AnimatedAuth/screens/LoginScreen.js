import { StatusBar } from 'expo-status-bar';
import React, {useState, useContext} from 'react';
import { StyleSheet, Text, View, Dimensions, TextInput, Pressable ,TouchableOpacity} from 'react-native';
import styles from './styles';
import Svg, { Image, Ellipse, ClipPath } from 'react-native-svg';
import Animated, {
  useSharedValue, 
  useAnimatedStyle, 
  withTiming, 
  interpolate,
  withDelay,
  runOnJS,
  withSequence,
  withSpring }from 'react-native-reanimated';
import { AuthContext } from '../context/AuthContext';

const LoginScreen = ({navigation}) => {
  const {height, width} = Dimensions.get('window');
  const imageposition = useSharedValue(1);
  const formButtonScale  = useSharedValue(1);
  const [isRegistering, setIsRegistering] = useState(false);
  const {login} = useContext(AuthContext);

  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [fullname,setFullname] = useState();
 
  const animatedStyle = useAnimatedStyle(() => {
    const interpolation = interpolate(imageposition.value, [0 , 1], [-height / 2 , 0])
    return{
      transform: [{translateY: withTiming(interpolation,{duration: 1000 })}]
    }

  })

  const buttonsAnimatedStyle = useAnimatedStyle (() => {
    const interpolation = interpolate(imageposition.value, [0 ,1],  [250,0 ])
    return{
      opacity:  withTiming(imageposition.value, {duration: 500}),
      transform:[{translateY: withTiming(interpolation, {duration:1000})}]
    }
  })

  const closeButtonAnimatedStyle = useAnimatedStyle(() => {
    const interpolation = interpolate(imageposition.value, [0, 1], [180,360])
     return{
      opacity: withTiming(imageposition.value === 1 ? 0 : 1, {duration: 800}),
      transform: [{rotate: withTiming(interpolation + "deg", {duration: 1000})}]
     }
  })

  const formAnimatedstyle = useAnimatedStyle (() => {
    return{
      opacity: imageposition.value === 0 ? withDelay(400, withTiming(1, {duration:800})) : withTiming(0, {duration: 300})
    }

  })

  const formButtonAnimatedStyle  = useAnimatedStyle(() => {
    return{
      transform: [{scale:formButtonScale.value }]
    }

  }) 

  const loginHandler = () => {
    imageposition.value = 0 
    if (isRegistering) {
      setIsRegistering(false);
      runOnJS(setIsRegistering)(false);
    }
  }

  const registerHandler = () => {
    imageposition.value = 0 
    if (!isRegistering) {
      setIsRegistering(true);
      runOnJS(setIsRegistering)(true);
    }
  }



  return (
    <Animated.View style={styles.container}>
      
      <Animated.View style={[StyleSheet.absoluteFill, animatedStyle]}> 
        <Svg height={height + 100} width={width}>
            <ClipPath id='clipPathId'>
            <Ellipse cx={width / 2 } rx={height} ry={height + 100} />
            </ClipPath>
            <Image 
            href={require('../assets/login-background.jpg')}
            height={height + 100}
            width = {width + 100}
            preserveAspectRatio="xMidYMid slice "
            clipPath='url(#clipPathId)'
            /> 
        </Svg>

        <Animated.View style={[styles.closeButtonContainer, closeButtonAnimatedStyle]}> 
            <Text onPress={() => imageposition.value = 1}>X</Text>
        </Animated.View>

      </Animated.View>


      <View style={styles.bottomcontainer}> 
            <Animated.View style={buttonsAnimatedStyle}>
                <Pressable style ={styles.button} onPress = {loginHandler}>
                <Text style= {styles.buttonText}> LOG IN</Text>
                </Pressable>
            </Animated.View>

            <Animated.View style={buttonsAnimatedStyle}>
                <Pressable style ={styles.button} onPress = {registerHandler}>
                <Text style= {styles.buttonText}> REGISTER</Text>
                </Pressable>
            </Animated.View>
        

            <Animated.View  style={[styles.formInputContainer, formAnimatedstyle]}>
                <TextInput 
                placeholder='Email '
                value={email}
                placeholderTextColor='black'
                style= {styles.textInput}
                onChangeText= {(text) => setEmail(text)}/>

                {isRegistering && (
                <TextInput
                placeholder='Full Name '
                value={fullname}
                placeholderTextColor='black'
                style= {styles.textInput}/> 
                
                )}
                <TextInput
                placeholder='Password '
                value={password}
                placeholderTextColor='black'
                style= {styles.textInput}
                secureTextEntry
                onChangeText= {(text) => setPassword(text)}/>
        
                <Animated.View style={[styles.formButton, formButtonAnimatedStyle]}> 
                    <Pressable onPress={() => {
                      if (!isRegistering)
                      {
                        navigation.navigate('Home')
                      }
                      else if(isRegistering){
                        navigation.navigate('Login')
                      }}}>
                        <Text style={styles.buttonText}>{isRegistering? 'REGISTER' : 'LOG IN'}</Text>
                    </Pressable>
                        
                </Animated.View>
            </Animated.View>
        </View>
      
    </Animated.View>
  );
};

export default LoginScreen;


