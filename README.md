# Surplus React Native Challenge

![](https://media.giphy.com/media/SipkT4QCPJc5c3ZXyT/giphy.gif)

## ℹ️ About The Project

This repo contains the project about wheather app which implement animation on login screen and call api wheather from https://openweathermap.org/api

It's made using React Native with Expo. Furthermore, the development of this poject is using iPhone 14 Pro Max- iOS 16.2 emulator.

## 🆕 Getting Started

1. Clone the repo
   ```sh
   git clone https://gitlab.com/nunik06/surplus-react-native-challenge.git
   ```
2. Install dependencies
   ```sh
   npm install
   ```
3. run project
   ```sh
   expo start
   ```
4. select emulator

   ```sh
   a (for android)
   ```

   ```sh
   i (for ios)
   ```
